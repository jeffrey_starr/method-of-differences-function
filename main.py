import dataclasses

import flask
import functions_framework
import mdifferences
from result import Result, Err, Ok
from werkzeug.datastructures.structures import ImmutableMultiDict
import re
import decimal


MAX_EXPR_LENGTH = 64
MAX_ROWS = 200


@dataclasses.dataclass
class RequestBody:
    expr: str
    meta: mdifferences.TableMeta


def validate_request(form: ImmutableMultiDict) -> Result[RequestBody, Exception]:
    """
    Extract and validate the request for input data for the explain_method_differences function

    Validations performed:
    - `expr` is a non-empty string and length is less than MAX_EXPR_LENGTH
    - `meta.initial`, which defaults to zero, is either empty or a number
    - `meta.step`, which defaults to one, is either empty or a non-zero number
    - `meta.rows`, which defaults to 100, is either empty or a positive integer

    :param form: a multiple dict containing the form data
    :return: Either a RequestBody if valid or an Exception
    """
    maybe_expr = form.get('expr')
    maybe_initial = form.get('meta.initial')
    maybe_step = form.get('meta.step')
    maybe_rows = form.get('meta.rows')

    if maybe_expr is not None and 0 < len(maybe_expr) < MAX_EXPR_LENGTH:
        expr: str = maybe_expr
    else:
        return Err(ValueError(f'Expression must have a length between 1 and {MAX_EXPR_LENGTH}, inclusive'))

    try:
        if maybe_initial is None or maybe_initial == '':
            initial = 0
        else:
            initial = float(maybe_initial)

        if maybe_step is None or maybe_step == '':
            step = 1
        else:
            step = float(maybe_step)

        if maybe_rows is None or maybe_rows == '':
            rows = 100
        else:
            rows = int(maybe_rows)
            if rows < 1 or rows > MAX_ROWS:
                raise ValueError(f'rows must be an integer between 1 and {MAX_ROWS}, inclusive')

        return Ok(RequestBody(expr, mdifferences.TableMeta(initial=initial, step=step, rows=rows)))
    except Exception as exc:
        return Err(exc)


def tonum(num_expr: str) -> str:
    """
    Convert a numerical expression to a nicer looking number


    :param num_expr:
    :return:
    """
    rational = re.compile(r'(?P<sign>-)?(?P<numer>\d+)/(?P<denom>\d+)')
    deci = re.compile(r'(?P<sign>-)?(?P<dec>\d+(\.\d+)?)')

    rat = rational.match(num_expr)
    dec = deci.match(num_expr)

    if rat:
        with decimal.localcontext(decimal.ExtendedContext) as ctx:
            d = decimal.Decimal(rat.group('numer'))
            if rat.group('sign'):
                d = d * -1
            d = d / decimal.Decimal(rat.group('denom'))

            return str(d + 0)
    elif dec:
        with decimal.localcontext(decimal.ExtendedContext) as ctx:
            d = decimal.Decimal(num_expr)
            # force rounding
            return str(d.normalize() + 0)
    else:
        return f'?{num_expr}'


def render(expl: mdifferences.Explanation, meta: mdifferences.TableMeta) -> str:
    """
    Render an Explanation into an HTML fragment

    :param expl: an Explanation (components) of the Method of Differences
    :param meta: TableMeta outlining the computation
    :return: an HTML fragment
    """
    flask.current_app.jinja_env.filters['tonum'] = tonum

    return flask.render_template('explanation.html', e=expl, meta=meta)


# Register an HTTP function with the Functions Framework
@functions_framework.http
def explain_method_differences(request: flask.Request):
    def compute(body: RequestBody) -> Result[(mdifferences.Explanation, mdifferences.TableMeta), Exception]:
        r_expl = mdifferences.explain_method(body.expr, body.meta)
        return r_expl.map(lambda expl: (expl, body.meta))

    if request.method == 'OPTIONS':
        headers = {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "POST",
            "Access-Control-Allow-Headers": "*",
            "Access-Control-Max-Age": "3600",
        }

        return "", 204, headers

    resp = validate_request(request.form)\
        .and_then(compute)\
        .map(lambda tup: render(tup[0], tup[1]))

    headers = {"Access-Control-Allow-Origin": "*"}

    match resp:
        case Err(e):
            return str(e), 400, headers
        case Ok(explanation):
            return explanation, 200, headers
