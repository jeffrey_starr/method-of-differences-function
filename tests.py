import unittest

import main
from main import RequestBody, validate_request
from mdifferences import TableMeta
from werkzeug.datastructures.structures import ImmutableMultiDict

class FormValidation(unittest.TestCase):

    expectedSimpleCase = RequestBody('x**2 + x + 41', TableMeta(initial=0, step=1, rows=100))

    def test_valid_simple_case_fully_populated(self):
        form = ImmutableMultiDict({
            'expr': 'x**2 + x + 41',
            'meta.initial': '0',
            'meta.step': '1',
            'meta.rows': '100'})
        actual = validate_request(form).ok()
        self.assertEqual(self.expectedSimpleCase, actual)

    def test_valid_simple_case_partially_populated(self):
        form = ImmutableMultiDict({'expr': 'x**2 + x + 41'})
        actual = validate_request(form).ok()
        self.assertEqual(self.expectedSimpleCase, actual)

    def test_invalid_step_must_be_non_zero(self):
        form = ImmutableMultiDict({
            'expr': 'x**2 + x + 41',
            'meta.step': '0'})
        actual = validate_request(form).err()
        self.assertEqual(ValueError, type(actual))


class ToNum(unittest.TestCase):
    def test_float(self):
        self.assertEqual('15', main.tonum('15'))
        self.assertEqual('-15.179', main.tonum('-15.179'))
        self.assertEqual('0.5', main.tonum('1/2'))
        self.assertEqual('-0.666666667', main.tonum('-2/3'))
        self.assertEqual('41', main.tonum('41.0000000'))
