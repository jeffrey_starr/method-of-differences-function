from google.cloud import functions_v2 as functions


def update_function():
    func_name = 'projects/sound-invention-412223/locations/us-central1/functions/method-differences'

    functions_client = functions.FunctionServiceClient()
    function = functions_client.get_function(name=func_name)

    print(f'Fetched function definition: {function}')
    updated = {'name': func_name, 'build_config':
        {'source': {'storage_source': {'bucket': 'ztoz', 'object_': 'method.zip'}}}}

    req_update = functions.UpdateFunctionRequest({'function': updated, 'update_mask':
        'buildConfig.source.storageSource.bucket,buildConfig.source.storageSource.object'})
    resp = functions_client.update_function(req_update)

    print(f'Update function response: {resp.result()}')


if __name__ == '__main__':
    update_function()
